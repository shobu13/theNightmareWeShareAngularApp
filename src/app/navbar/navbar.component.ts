import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  private appLinksLogged = [
    {
      title: 'Accueil',
      url: '/home',
    },
    {
      title: 'Déconnexion',
      url: '/auth/logout',
    },
  ];

  private appLinksNotLogged = [
    {
      title: 'Accueil',
      url: '/home',
    },
    {
      title: 'Connexion',
      url: '/auth',
    }
  ];

  private appLinks = this.appLinksNotLogged;

  constructor() {
  }

  ngOnInit() {
  }

}
